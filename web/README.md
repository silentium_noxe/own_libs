URI class (v:1)
=================
Class model which help work with URIs. Has inner builder.
Example of usage:
```java
class Test{
    public static void main(String[] args){
        URI uri = URI.builder() text
            .scheme("http")
            .host("0.0.0.0")
            .port("8080")
            .path("example")
            .params(new HashMap<>(){{
                put("serviceClass", CountriesPort.class.getName());
            }})
            .build();
    }
}
```
View class (v:2)
==================
Class model which help work with MVC.
Contain class ViewParams for configure work with View.
Examples of usage:
```java
class Test{
	public static void main(String[] args){
		View view = new View();
		view.addFromFile("file_name");html_tags
		view.addFromFile("file_name", "special_extension");//<-- overload method with parameter "extension"
		view.add("name_tempalte", html_tags);//<-- add hard template
		view.update("name_template", "new_element");
		view.remove("name_template");
		String response = view.render();
	}
}
```
Methods **addFromFile** and **add** return object of type View:
```java
class Test{
	public static void main(String[] args){
		View view = new View();
		view.add("<html>")
			.add("<head></head>")
			.add("<body>")
			.addFromFile("example")
			.add("</body>")
			.add("</html>");
		String result = view.render();
	}
}
```
Class ViewParams contain next params (access through getter/setter):<br>
- fileSeparator (default = "/")
- pathResourcesDir (default = "./src/main/resources/") Directory which contain templates
- defaultFileExtension (default = "html")

HtmlPageBuilder (v:1)
=====================
Class builder HTML documents.<br>
Use for generating any HTML pages. Example "Hello World":
```java
class Test{
    public static void main(String[] args){
      HtmlPageBuilder builder = new HtmlPageBuilder();
              builder.setTitle("Hello World!")//<-- Put into <head> tag <title>
                      .setCharset("UTF-8")//<-- Put into <head> tag <meta charset='UTF-8'>
                      .putIntoBody(new SimpleText("Hello World!"));//<-- Put into <body> just text
      
              String result = builder.build();
    }
}
```
#### Building page
Class HtmlPageBuilder working with class Tag and other who extending him.<br>
If you can't to find needed Tag in a package "\*.web.html.tags" just create own class which will extend class \*.web.html.tags.Tag.
If your own tag have some attributes then use annotation "Attribute".

For putting tag into head use `putIntoHead()`, for body `putIntoBody()`. Also you have fast methods like `addLink()` 
which will add tag `<link>` and `addMeta()` which will add `<meta>`.

If you need fast getting new Form, Table or List then use methods: `buildForm()`, `buildTable()`, `buildOrderedList()` or `buildUnorderedList()`. 
For example:
```java
String[][] matrix = {
        {"id", "key", "value"},
        {"0", "hello", "world"},
        {"1", "some", "text"},
        {"2", "I", "think"},
        {"3", "must", "work"}
};

HtmlPageBuilder builder = new HtmlPageBuilder();
builder.putIntoBody(builder.buildTable(matrix));
```

When you putted some element you have easy way to get last added tag via `getLastAddedTag()` and `getElementById()`.