package com.silentium_noxe.java.web.html.tags;

import com.silentium_noxe.java.web.html.Attribute;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.lang.reflect.Field;
import java.util.*;

/**
 * @author silentium_noxe
 */
@Getter
@Setter
public class Tag {
    private final String nameTag;
    private List<Tag> childs;
    private Set<String> idList;
    private Set<Class<? extends Tag>> bannedTags;

    /**
     * @see <a href="http://htmlbook.ru/html/attr/class">htmlbook.ru</a>
     */
    @Attribute
    private Set<String> attrClasses;

    /**
     * @see <a href="http://htmlbook.ru/html/attr/contenteditable">htmlbook.ru</a>
     */
    @Attribute
    private Boolean contenteditable;

    /**
     * @see <a href="http://htmlbook.ru/html/attr/contextmenu">htmlbook.ru</a>
     */
    @Attribute
    private String contextmenu;

    /**
     * @see <a href="http://htmlbook.ru/html/attr/dir">htmlbook.ru</a>
     */
    @Attribute
    private String dir;

    /**
     * @see <a href="http://htmlbook.ru/html/attr/hidden">htmlbook.ru</a>
     */
    @Attribute
    private Boolean hidden;

    /**
     * @see <a href="http://htmlbook.ru/html/attr/accesskey">htmlbook.ru</a>
     */
    @Attribute
    private String accesskey;

    /**
     * @see <a href="http://htmlbook.ru/html/attr/id">htmlbook.ru</a>
     */
    @Attribute
    private String id;

    /**
     * @see <a href="http://htmlbook.ru/html/attr/lang">htmlbook.ru</a>
     */
    @Attribute
    private String lang;

    /**
     * @see <a href="http://htmlbook.ru/html/attr/spellcheck">htmlbook.ru</a>
     */
    @Attribute
    private Boolean spellcheck;

    /**
     * @see <a href="http://htmlbook.ru/html/attr/styles">htmlbook.ru</a>
     */
    @Attribute
    private Map<String, String> styles;

    /**
     * @see <a href="http://htmlbook.ru/html/attr/tabindex">htmlbook.ru</a>
     */
    @Attribute
    private Integer tabindex;

    /**
     * @see <a href="http://htmlbook.ru/html/attr/title">htmlbook.ru</a>
     */
    @Attribute
    private String title;

    public Tag(String nameTag){
        if (isWrong(nameTag)){
            throw new IllegalArgumentException("Name of tag can't be null or empty");
        }

        this.nameTag = nameTag;
        idList = new HashSet<>();
        bannedTags = new HashSet<>();
    }

    protected void banTag(Class<? extends Tag> tag){
        bannedTags.add(tag);
    }

    protected void banTags(Set<Class<? extends Tag>> tags){
        bannedTags.addAll(tags);
    }

    protected <T extends Tag> void addChild(T tag) throws IllegalArgumentException{
        if (isWrong(tag)){
            throw new IllegalArgumentException("Parameter can't be null");
        }

        if (bannedTags.contains(tag.getClass())){
            throw new IllegalArgumentException(this.getClass().getSimpleName() + " can't contain "+tag.getClass().getSimpleName());
        }

        saveId(tag.getId());

        if (childs == null){
            childs = new ArrayList<>();
        }

        childs.add(tag);
    }

    protected void removeChild(Tag tag){
        if (childs == null){
            return;
        }

        if (!isWrong(tag)){
            idList.remove(tag.getId());
        }

        childs.remove(tag);
    }

    public void addClass(String atrClass){
        if (isWrong(atrClass)){
            return;
        }

        attrClasses.add(atrClass);
    }

    public void addStyle(String key, String value){
        if (isWrong(key) || isWrong(value)){
            return;
        }

        if (styles == null){
            styles = new HashMap<>();
        }

        styles.put(key, value);
    }

    protected void saveId(String id){
        if (!isWrong(id)){
            if(containId(id)){
                throw new IllegalArgumentException("ID "+id+" already exist");
            }
            idList.add(id);
        }
    }

    public boolean containId(String id){
        if(idList == null || childs == null){
            return false;
        }

        if(idList.contains(id)){
            return true;
        }

        for(Tag child : childs){
            if(child.containId(id)){
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return getOpeningTag() +
                getChildTagsAsString() +
                getClosingTag();
    }

    protected String getOpeningTag(){
        String attributes = getAttributesAsString().trim();
        if(!attributes.isEmpty()){
            attributes = " " + attributes;
        }
        return "<"+ nameTag +attributes+">";
    }

    protected String getClosingTag(){
        return "</"+ nameTag +">";
    }

    protected String getChildTagsAsString(){
        StringBuilder builder = new StringBuilder();
        if (childs != null) {
            for (Tag tag : childs) {
                builder.append(tag);
            }
        }
        return builder.toString();
    }

    public String getClassesAsString(){
        if (attrClasses == null){
            return "";
        }

        StringBuilder builder = new StringBuilder();

        for (String atrClass : attrClasses){
            builder.append(atrClass).append(" ");
        }

        return builder.toString().trim();
    }

    public String getStylesAsString(){
        if (styles == null){
            return "";
        }

        StringBuilder builder = new StringBuilder();

        for (String key : styles.keySet()){
            builder.append(" ").append(key).append(": ").append(styles.get(key));
        }

        return builder.toString();
    }

    protected String getAttributesAsString(){
        try{
            return " " + searchAttributes(this.getClass(), new StringBuilder()).trim();
        } catch(IllegalAccessException e){
            e.printStackTrace();
            return "";
        }
    }

    private String searchAttributes(Class parentClass, StringBuilder builder) throws IllegalAccessException{
        for(Field field : parentClass.getDeclaredFields()){
            if(field.isAnnotationPresent(Attribute.class)){
                Attribute anotation = field.getAnnotation(Attribute.class);

                field.setAccessible(true);

                if(field.getName().equals("atrClasses")){
                    builder.append(" ").append(getClassesAsString());
                    field.setAccessible(false);
                    continue;
                }

                if(field.getName().equals("styles")){
                    builder.append(" ").append(getStylesAsString());
                    field.setAccessible(false);
                    continue;
                }

                Object value = field.get(this);
                if(!isWrong(value)){
                    if(anotation.withoutValue()){
                        if ((Boolean) value) {
                            builder.append(" ").append(field.getName().replace("_", "-"));
                        }
                    }else{
                        builder.append(" ").append(attributeToString(field.getName().replace("_", "-"), (String) value));
                    }
                }
                field.setAccessible(false);
            }
        }

        if(parentClass != Tag.class){
            return searchAttributes(parentClass.getSuperclass(), builder);
        }

        return builder.toString();
    }

    protected boolean isWrong(Object object){
        if (object == null){
            return true;
        }

        if (object instanceof String){
            String string = (String)object;
            return string.isEmpty();
        }

        if (object instanceof Collection){
            Collection collection = (Collection) object;
            return collection.isEmpty();
        }

        return false;
    }

    protected String attributeToString(String atrName, String value){
        return atrName+"='"+value+"'";
    }
}
