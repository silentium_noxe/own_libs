package com.silentium_noxe.java.web.html.tags;

import com.silentium_noxe.java.web.html.Attribute;
import lombok.Getter;
import lombok.Setter;

/**
 * @see <a href="http://htmlbook.ru/html/th">htmlbook.ru</a>
 * @author silentium_noxe
 */
@Getter
@Setter
public class TableHead extends Tag{

    /**
     * @see <a href="http://htmlbook.ru/html/th/abbr">htmlbook.ru</a>
     */
    @Attribute
    private String abbr;

    /**
     * @see <a href="http://htmlbook.ru/html/th/align">htmlbook.ru</a>
     */
    @Attribute
    private String align;

    /**
     * @see <a href="http://htmlbook.ru/html/th/axis">htmlbook.ru</a>
     */
    @Attribute
    private String axis;

    /**
     * @see <a href="http://htmlbook.ru/html/th/background">htmlbook.ru</a>
     */
    @Attribute
    private String background;

    /**
     * @see <a href="http://htmlbook.ru/html/th/bgcolor">htmlbook.ru</a>
     */
    @Attribute
    private String bgcolor;

    /**
     * @see <a href="http://htmlbook.ru/html/th/bordercolor">htmlbook.ru</a>
     */
    @Attribute
    private String bordercolor;

    /**
     * @see <a href="http://htmlbook.ru/html/th/char">htmlbook.ru</a>
     */
    @Attribute
    private String attrChar;

    /**
     * @see <a href="http://htmlbook.ru/html/th/charoff">htmlbook.ru</a>
     */
    @Attribute
    private Integer charoff;

    /**
     * @see <a href="http://htmlbook.ru/html/th/colspan">htmlbook.ru</a>
     */
    @Attribute
    private Integer colspan;

    /**
     * @see <a href="http://htmlbook.ru/html/th/headers">htmlbook.ru</a>
     */
    @Attribute
    private String headers;

    /**
     * @see <a href="http://htmlbook.ru/html/th/height">htmlbook.ru</a>
     */
    @Attribute
    private Integer height;

    /**
     * @see <a href="http://htmlbook.ru/html/th/nowrap">htmlbook.ru</a>
     */
    @Attribute(withoutValue = true)
    private Boolean nowrap;

    /**
     * @see <a href="http://htmlbook.ru/html/th/rowspan">htmlbook.ru</a>
     */
    @Attribute
    private Integer rowspan;

    /**
     * @see <a href="http://htmlbook.ru/html/th/scope">htmlbook.ru</a>
     */
    @Attribute
    private String scope;

    /**
     * @see <a href="http://htmlbook.ru/html/th/valign">htmlbook.ru</a>
     */
    @Attribute
    private String valign;

    /**
     * @see <a href="http://htmlbook.ru/html/th/width">htmlbook.ru</a>
     */
    @Attribute
    private Integer width;

    private SimpleText text;

    public TableHead(){
        this("");
    }

    public TableHead(String text){
        this(new SimpleText(text));
    }

    public TableHead(SimpleText text){
        super("th");
        this.text = text;
        addChild(text);
    }

    public void setText(String text){
        setText(new SimpleText(text));
    }

    public void setText(SimpleText value){
        removeChild(this.text);
        this.text = value;
        addChild(this.text);
    }
}
