package com.silentium_noxe.java.web.html.tags;

import com.silentium_noxe.java.web.html.Attribute;
import lombok.Getter;
import lombok.Setter;

/**
 * @see <a href="http://htmlbook.ru/html/h1">htmlbook.ru</a>
 * @author silentium_noxe
 */
public class Headline extends Tag {

    @Getter
    @Setter
    private int level;

    @Getter
    private SimpleText text;

    /**
     * @see <a href="http://htmlbook.ru/html/h1/align">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String align;

    private static int changeLevelIfWrong(int level){
        if(level < 1){
            level = 1;
        }else if(level > 6){
            level = 6;
        }
        return level;
    }

    public Headline() {
        this(1);
    }

    public Headline(int level) {
        this(level, "");
    }

    public Headline(int level, String text){
        this(level, new SimpleText(text));
    }

    public Headline(int level, SimpleText text){
        super("h");
        this.text = text;
        this.level = level;
        addChild(this.text);
    }

    public void setText(String text){
        setText(new SimpleText(text));
    }

    public void setText(SimpleText value){
        removeChild(this.text);
        this.text = value;
        addChild(this.text);
    }

    @Override
    protected String getOpeningTag() {
        return "<"+getNameTag()+changeLevelIfWrong(level)+getAttributesAsString()+">";
    }
}
