package com.silentium_noxe.java.web.html.tags;

import com.silentium_noxe.java.web.html.Attribute;
import lombok.Getter;
import lombok.Setter;

/**
 * @see <a href="http://htmlbook.ru/html/li">htmlbook.ru</a>
 * @author silentium_noxe
 */
public class ListItem extends Tag{

    /**
     * @see <a href="http://htmlbook.ru/html/li/type">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String type;

    /**
     * @see <a href="http://htmlbook.ru/html/li/value">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private Integer value;

    public ListItem() {
        super("li");
    }

    public ListItem(Tag tag){
        super("li");
        addChild(tag);
    }

    @Override
    public  <T extends Tag> void addChild(T tag){
        super.addChild(tag);
    }
}
