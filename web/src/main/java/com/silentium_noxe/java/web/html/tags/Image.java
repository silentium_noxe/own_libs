package com.silentium_noxe.java.web.html.tags;

import com.silentium_noxe.java.web.html.Attribute;
import lombok.Getter;
import lombok.Setter;

/**
 * @see <a href="http://htmlbook.ru/html/img">htmlbook.ru</a>
 * @author silentium_noxe
 */
@Getter
@Setter
public class Image extends Tag {

    /**
     * @see <a href="http://htmlbook.ru/html/img/align">htmlbook.ru</a>
     */
    @Attribute
    private String align;

    /**
     * @see <a href="http://htmlbook.ru/html/img/alt">htmlbook.ru</a>
     */
    @Attribute
    private String alt;

    /**
     * @see <a href="http://htmlbook.ru/html/img/border">htmlbook.ru</a>
     */
    @Attribute
    private String border;

    /**
     * @see <a href="http://htmlbook.ru/html/img/height">htmlbook.ru</a>
     */
    @Attribute
    private String height;

    /**
     * @see <a href="http://htmlbook.ru/html/img/hspace">htmlbook.ru</a>
     */
    @Attribute
    private String hspace;

    /**
     * @see <a href="http://htmlbook.ru/html/img/ismap">htmlbook.ru</a>
     */
    @Attribute
    private String ismap;

    /**
     * @see <a href="http://htmlbook.ru/html/img/longdesc">htmlbook.ru</a>
     */
    @Attribute
    private String longdesc;

    /**
     * @see <a href="http://htmlbook.ru/html/img/lowsrc">htmlbook.ru</a>
     */
    @Attribute
    private String lowsrc;

    /**
     * @see <a href="http://htmlbook.ru/html/img/src">htmlbook.ru</a>
     */
    @Attribute
    private String src;

    /**
     * @see <a href="http://htmlbook.ru/html/img/vspace">htmlbook.ru</a>
     */
    @Attribute
    private String vspace;

    /**
     * @see <a href="http://htmlbook.ru/html/img/width">htmlbook.ru</a>
     */
    @Attribute
    private String width;

    /**
     * @see <a href="http://htmlbook.ru/html/img/usemap">htmlbook.ru</a>
     */
    @Attribute
    private String usemap;

    public Image() {
        super("img");
    }

    @Override
    public String toString() {
        return getOpeningTag();
    }
}
