package com.silentium_noxe.java.web.html.tags;

import lombok.Getter;

/**
 * @see <a href="http://htmlbook.ru/html/b">htmlbook.ru</a>
 * @author silentium_noxe
 */
public class BoldText extends Tag {
    @Getter
    private SimpleText text;

    public BoldText() {
        this("");
    }

    public BoldText(String value){
        this(new SimpleText(value));
    }

    public BoldText(SimpleText value){
        super("b");
        this.text = value;
        addChild(this.text);
    }

    public void setText(String text){
        setText(new SimpleText(text));
    }

    public void setText(SimpleText value){
        removeChild(this.text);
        this.text = value;
        addChild(this.text);
    }
}
