package com.silentium_noxe.java.web.html.tags;


import com.silentium_noxe.java.web.html.Attribute;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @see <a href="http://htmlbook.ru/html/ol">htmlbook.ru</a>
 * @author silentium_noxe
 */
@Getter
@Setter
public class OrderedList extends Tag {

    /**
     * @see <a href="http://htmlbook.ru/html/ol/type">htmlbook.ru</a>
     */
    @Attribute
    private String type;

    /**
     * @see <a href="http://htmlbook.ru/html/ol/reversed">htmlbook.ru</a>
     */
    @Attribute(withoutValue = true)
    private Boolean reversed;

    /**
     * @see <a href="http://htmlbook.ru/html/ol/start">htmlbook.ru</a>
     */
    @Attribute
    private Integer start;

    public OrderedList() {
        super("ol");
    }

    public OrderedList(List<ListItem> itemList){
        super("ol");

        for (ListItem item : itemList){
            add(item);
        }
    }

    public void add(Tag tag){
        if (tag instanceof ListItem){
            addChild(tag);
        } else {
            addChild(new ListItem(tag));
        }
    }
}
