package com.silentium_noxe.java.web.html.tags;

import com.silentium_noxe.java.web.html.Attribute;
import lombok.Getter;
import lombok.Setter;

/**
 * @see <a href="http://htmlbook.ru/html/input">htmlbook.ru</a>
 * @author silentium_noxe
 */
@Getter
@Setter
public class Input extends Tag {

    /**
     * @see <a href="http://htmlbook.ru/html/input/accept">htmlbook.ru</a>
     */
    @Attribute
    private String accept;

    /**
     * @see <a href="http://htmlbook.ru/html/input/accesskey">htmlbook.ru</a>
     */
    @Attribute
    private String accesskey;

    /**
     * @see <a href="http://htmlbook.ru/html/input/align">htmlbook.ru</a>
     */
    @Attribute
    private String align;

    /**
     * @see <a href="http://htmlbook.ru/html/input/alt">htmlbook.ru</a>
     */
    @Attribute
    private String alt;

    /**
     * @see <a href="http://htmlbook.ru/html/input/autocomplete">htmlbook.ru</a>
     */
    @Attribute
    private String autocomplete;

    /**
     * @see <a href="http://htmlbook.ru/html/input/autofocus">htmlbook.ru</a>
     */
    @Attribute(withoutValue = true)
    private Boolean autofocus;

    /**
     * @see <a href="http://htmlbook.ru/html/input/border">htmlbook.ru</a>
     */
    @Attribute
    private Integer border;

    /**
     * @see <a href="http://htmlbook.ru/html/input/checked">htmlbook.ru</a>
     */
    @Attribute(withoutValue = true)
    private Boolean checked;

    /**
     * @see <a href="http://htmlbook.ru/html/input/disabled">htmlbook.ru</a>
     */
    @Attribute(withoutValue = true)
    private Boolean disabled;

    /**
     * @see <a href="http://htmlbook.ru/html/input/form">htmlbook.ru</a>
     */
    @Attribute
    private String form;

    /**
     * @see <a href="http://htmlbook.ru/html/input/formaction">htmlbook.ru</a>
     */
    @Attribute
    private String formaction;

    /**
     * @see <a href="http://htmlbook.ru/html/input/formenctype">htmlbook.ru</a>
     */
    @Attribute
    private String formenctype;

    /**
     * @see <a href="http://htmlbook.ru/html/input/formmethod">htmlbook.ru</a>
     */
    @Attribute
    private String formmethod;

    /**
     * @see <a href="http://htmlbook.ru/html/input/formnovalidate">htmlbook.ru</a>
     */
    @Attribute(withoutValue = true)
    private Boolean formnovalidate;

    /**
     * @see <a href="http://htmlbook.ru/html/input/formtarget">htmlbook.ru</a>
     */
    @Attribute
    private String formtarget;

    /**
     * @see <a href="http://htmlbook.ru/html/input/list">htmlbook.ru</a>
     */
    @Attribute
    private String list;

    /**
     * @see <a href="http://htmlbook.ru/html/input/max">htmlbook.ru</a>
     */
    @Attribute
    private Integer max;

    /**
     * @see <a href="http://htmlbook.ru/html/input/maxlength">htmlbook.ru</a>
     */
    @Attribute
    private Integer maxlength;

    /**
     * @see <a href="http://htmlbook.ru/html/input/min">htmlbook.ru</a>
     */
    @Attribute
    private Integer min;

    /**
     * @see <a href="http://htmlbook.ru/html/input/multiple">htmlbook.ru</a>
     */
    @Attribute(withoutValue = true)
    private Boolean multiple;

    /**
     * @see <a href="http://htmlbook.ru/html/input/name">htmlbook.ru</a>
     */
    @Attribute
    private String name;

    /**
     * @see <a href="http://htmlbook.ru/html/input/pattern">htmlbook.ru</a>
     */
    @Attribute
    private String pattern;

    /**
     * @see <a href="http://htmlbook.ru/html/input/placeholder">htmlbook.ru</a>
     */
    @Attribute
    private String placeholder;

    /**
     * @see <a href="http://htmlbook.ru/html/input/readonly">htmlbook.ru</a>
     */
    @Attribute(withoutValue = true)
    private Boolean readonly;

    /**
     * @see <a href="http://htmlbook.ru/html/input/required">htmlbook.ru</a>
     */
    @Attribute(withoutValue = true)
    private Boolean required;

    /**
     * @see <a href="http://htmlbook.ru/html/input/size">htmlbook.ru</a>
     */
    @Attribute
    private String size;

    /**
     * @see <a href="http://htmlbook.ru/html/input/src">htmlbook.ru</a>
     */
    @Attribute
    private String src;

    /**
     * @see <a href="http://htmlbook.ru/html/input/step">htmlbook.ru</a>
     */
    @Attribute
    private String step;

    /**
     * @see <a href="http://htmlbook.ru/html/input/tabindex">htmlbook.ru</a>
     */
    @Attribute
    private Integer tabindex;

    /**
     * @see <a href="http://htmlbook.ru/html/input/type">htmlbook.ru</a>
     */
    @Attribute
    private String type;

    /**
     * @see <a href="http://htmlbook.ru/html/input/value">htmlbook.ru</a>
     */
    @Attribute
    private String value;

    public Input() {
        super("input");
    }

    public Input(String name, String type){
        this(name, type, false);
    }

    public Input(String name, String type, boolean required){
        super("input");
        if (name != null && !name.isEmpty()){
            this.name = name;
        }

        if (type != null && !type.isEmpty()){
            this.type = type;
        }

        this.required = required;
    }

    public void setAutocomplete(boolean value){
        autocomplete = value ? "on" : "off";
    }

    @Override
    public String toString() {
        return getOpeningTag();
    }
}
