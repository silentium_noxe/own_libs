package com.silentium_noxe.java.web.html.tags;

import java.util.HashMap;
import java.util.Map;

/**
 * @see <a href="http://htmlbook.ru/html/p">htmlbook.ru</a>
 * @author silentium_noxe
 * Use [name_varible] for building dynamic text
 */
public class SimpleText extends Tag {
    private String text;

    private Map<String, Tag> variables;

    public SimpleText(String text) {
        this(text, new HashMap<>());
    }

    public SimpleText(String text, Map<String, Tag> variables){
        super("simple_text");

        this.text = text;
        this.variables = variables;
    }

    public void set(String text){
        this.text = text;
    }

    public String getText(){
        return text;
    }

    public void addVariable(String name, Tag value){
        variables.put(name, value);
    }

    @Override
    public String toString() {
        return includeVariables();
    }

    private String includeVariables(){
        String result = text;
        for(String nameVariable : variables.keySet()){
            result = result.replace("["+nameVariable+"]", variables.get(nameVariable).toString());
        }
        result = result.replace("/]", "]");
        return result;
    }
}
