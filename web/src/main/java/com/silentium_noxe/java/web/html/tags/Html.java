package com.silentium_noxe.java.web.html.tags;

import com.silentium_noxe.java.web.html.Attribute;
import lombok.Getter;
import lombok.Setter;

/**
 * @see <a href="http://htmlbook.ru/html/html">htmlbook.ru</a>
 * @author silentium_noxe
 */
public class Html extends Tag {

    private Head head;
    private Body body;

    /**
     * @see <a href="http://htmlbook.ru/html/html/title">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String title;

    /**
     * @see <a href="http://htmlbook.ru/html/html/manifest">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String manifest;

    /**
     * @see <a href="http://htmlbook.ru/html/html/xmlns">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String xmlns;

    public Html(){
        super("html");
        head = new Head();
        body = new Body();
    }

    public Head getHead(){
        return head;
    }

    public Body getBody(){
        return body;
    }

    public void setHead(Head head){
        this.head = head;
    }

    public void setBody(Body body){
        this.body = body;
    }

    public <T extends Tag> void putIntoHead(T tag){
        head.addChild(tag);
    }

    public <T extends Tag> void putIntoBody(T tag){
        body.addChild(tag);
    }

    @Override
    public String toString(){
        return  getOpeningTag()+
                head+
                body+
                getClosingTag();
    }
}
