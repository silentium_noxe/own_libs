package com.silentium_noxe.java.web.html;

import com.silentium_noxe.java.web.html.tags.*;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author silentium_noxe
 * TODO: implement event listeners
 */
public class HtmlPageBuilder {
    private Html html;

    @Getter
    private Tag lastAddedTag;

    public HtmlPageBuilder(){
        html = new Html();
    }

    public HtmlPageBuilder setTitle(String title){
        html.getHead().addChild(new Title(title));
        return this;
    }

    public HtmlPageBuilder setCharset(String charset){
        Meta meta = new Meta();
        meta.setCharset(charset);
        html.putIntoHead(meta);
        return this;
    }

    public HtmlPageBuilder addMeta(Meta meta){
        html.putIntoHead(meta);
        return this;
    }

    public HtmlPageBuilder addMeta(String name, String content){
        Meta meta = new Meta();
        meta.setName(name);
        meta.setContent(content);
        html.putIntoHead(meta);
        return this;
    }

    public HtmlPageBuilder addMetaHttpEquiv(String http_equiv, String content){
        Meta meta = new Meta();
        meta.setHttp_equiv(http_equiv);
        meta.setContent(content);
        html.putIntoHead(meta);
        return this;
    }

    public HtmlPageBuilder addLink(Link link){
        html.putIntoHead(link);
        return this;
    }

    public HtmlPageBuilder addLink(String rel, String href){
        Link link = new Link();
        link.setRel(rel);
        link.setHref(href);
        html.putIntoHead(link);
        return this;
    }

    public HtmlPageBuilder addLink(String rel, String href, String type){
        Link link = new Link();
        link.setRel(rel);
        link.setHref(href);
        link.setType(type);
        html.putIntoHead(link);
        return this;
    }

    public <T extends Tag> HtmlPageBuilder putIntoHead(T tag){
        html.putIntoHead(tag);
        lastAddedTag = tag;
        return this;
    }

    public <T extends Tag> HtmlPageBuilder putIntoBody(T tag){
        html.putIntoBody(tag);
        lastAddedTag = tag;
        return this;
    }

    public Table buildTable(TableRow head, List<TableRow> dataList){
        Table table = new Table();

        table.setHead(head);
        table.addRows(dataList);

        return table;
    }

    public Table buildTable(String[] headers, String[][] dataList){
        Table table = new Table();

        table.setHead(headers);

        for (String[] row : dataList){
            table.addRow(row);
        }

        return table;
    }

    public Table buildTable(String[][] matrix){
        Table table = new Table();
        table.setHead(matrix[0]);

        for(int i = 1; i < matrix.length; i++){
            table.addRow(matrix[i]);
        }

        return table;
    }

    public Form buildForm(String method, String action, Map<String, String> nameTypeMapFields){
        List<Input> inputs = new ArrayList<>();
        for(String name : nameTypeMapFields.keySet()){
            inputs.add(new Input(name, nameTypeMapFields.get(name)));
        }

        return buildForm(method, action, inputs);
    }

    public Form buildForm(String method, String action, List<Input> inputs){
        Form form = new Form(method, action);

        for (Input input : inputs){
            form.addInput(input);
        }

        return form;
    }

    public UnorderedList buildUnorderedList(String ... data){
        UnorderedList list = new UnorderedList();

        for (String str : data){
            list.add(new ListItem(new SimpleText(str)));
        }

        return list;
    }

    public UnorderedList buildUnorderedList(List<ListItem> items){
        UnorderedList list = new UnorderedList();

        for (ListItem item : items){
            list.add(item);
        }

        return list;
    }

    public OrderedList buildOrderedList(String ... data){
        OrderedList list = new OrderedList();

        for (String str : data){
            list.add(new ListItem(new SimpleText(str)));
        }

        return list;
    }

    public OrderedList buildOrderedList(List<ListItem> items){
        OrderedList list = new OrderedList();

        for (ListItem item : items){
            list.add(item);
        }

        return list;
    }

    public Head getHead(){
        return html.getHead();
    }

    public Body getBody(){
        return html.getBody();
    }

    //TODO: implement parallel searching
    public Tag getElementById(String searchId){
        if(getHead().getChilds() != null){
            for(Tag tag : getHead().getChilds()){
                if(tag.getId().equals(searchId)){
                    return tag;
                }

                if(tag.containId(searchId)){
                    return searchElementByIdInTag(tag, searchId);
                }
            }
        }

        if(getBody().getChilds() != null){
            for(Tag tag : getBody().getChilds()){
                if(tag.getId().equals(searchId)){
                    return tag;
                }
                if(tag.containId(searchId)){
                    if(tag.containId(searchId)){
                        return searchElementByIdInTag(tag, searchId);
                    }
                }
            }
        }

        return null;
    }

    private Tag searchElementByIdInTag(Tag parent, String searchId){
        for(Tag child : parent.getChilds()){
            if(child.getId().equals(searchId)){
                return child;
            }

            Tag searchTag;
            if(child.containId(searchId)){
                if((searchTag = searchElementByIdInTag(child, searchId)) != null){
                    return searchTag;
                }
            }
        }
        return null;
    }

    public String build(){
        return "<!DOCTYPE html>"+html.toString();
    }
}
