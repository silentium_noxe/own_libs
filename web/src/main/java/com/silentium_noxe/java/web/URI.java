package com.silentium_noxe.java.web;

import lombok.*;

import java.util.Map;

/**
 * @author silentium_noxe
 */
@Builder
@Getter
@Setter
public class URI {
    public static final String PATH_SEPARATOR = "/";
    public static final String SCHEME_SEPARATOR = "://";
    public static final String PARAM_SEPARATOR = "&";
    public static final String PATH_PARAM_SEPARATOR = "?";

    private String scheme;
    private String host;
    private String port;
    private String path;

    @Singular
    private Map<String, String> params;

    @Override
    public String toString() {
        StringBuilder paramsString = new StringBuilder(PATH_PARAM_SEPARATOR);
        for(String key : params.keySet()){
            paramsString.append(key).append("=").append(params.get(key)).append(PARAM_SEPARATOR);
        }
        paramsString.deleteCharAt(paramsString.length()-1);

        if (port != null && !port.isEmpty()){
            port = ":"+port;
        }

        if (path != null && !path.isEmpty()){
            if (path.indexOf("/") == 0){
                path = path.substring(1);
            }
        }

        return scheme+SCHEME_SEPARATOR+host+port+PATH_SEPARATOR+path+paramsString.toString();
    }
}
