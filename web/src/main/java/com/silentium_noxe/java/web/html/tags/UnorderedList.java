package com.silentium_noxe.java.web.html.tags;

import com.silentium_noxe.java.web.html.Attribute;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @see <a href="http://htmlbook.ru/html/ul">htmlbook.ru</a>
 * @author silentium_noxe
 */
public class UnorderedList extends Tag {

    /**
     * @see <a href="http://htmlbook.ru/html/ul/type">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String type;

    public UnorderedList() {
        super("ul");
    }

    public UnorderedList(List<ListItem> itemList){
        super("ul");

        for (ListItem item : itemList){
            add(item);
        }
    }

    public void add(Tag tag){
        if (tag instanceof ListItem){
            addChild(tag);
        } else {
            addChild(new ListItem(tag));
        }
    }
}
