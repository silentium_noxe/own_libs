package com.silentium_noxe.java.web.html.tags;

import com.silentium_noxe.java.web.html.Attribute;
import lombok.Getter;
import lombok.Setter;

/**
 * @see <a href="http://htmlbook.ru/html/meta">htmlbook.ru</a>
 * @author silentium_noxe
 */
@Getter
@Setter
public class Meta extends Tag {

    /**
     * @see <a href="http://htmlbook.ru/html/meta/charset">htmlbook.ru</a>
     */
    @Attribute
    private String charset;

    /**
     * @see <a href="http://htmlbook.ru/html/meta/content">htmlbook.ru</a>
     */
    @Attribute
    private String content;

    /**
     * @see <a href="http://htmlbook.ru/html/meta/http-equiv">htmlbook.ru</a>
     */
    @Attribute
    private String http_equiv;

    /**
     * @see <a href="http://htmlbook.ru/html/meta/name">htmlbook.ru</a>
     */
    @Attribute
    private String name;

    public Meta(){
        super("meta");
    }

    @Override
    public String toString() {
        return getOpeningTag();
    }
}
