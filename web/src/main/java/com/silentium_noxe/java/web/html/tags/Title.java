package com.silentium_noxe.java.web.html.tags;

import lombok.Getter;
import lombok.Setter;

/**
 * @see <a href="http://htmlbook.ru/html/title">htmlbook.ru</a>
 */
public class Title extends Tag {
    @Getter
    @Setter
    private String value;

    public Title(){
        this(null);
    }

    public Title(String value){
        super("title");
        this.value = value;
    }

    @Override
    public String toString(){
        return  getOpeningTag()+
                value+
                getClosingTag();
    }
}
