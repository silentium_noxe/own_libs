package com.silentium_noxe.java.web.html.tags;

import com.silentium_noxe.java.web.html.Attribute;
import lombok.Getter;
import lombok.Setter;

/**
 * @see <a href="http://htmlbook.ru/html/form">htmlbook.ru</a>
 * @author silentium_noxe
 */
@Getter
@Setter
public class Form extends Tag {

    /**
     * @see <a href="http://htmlbook.ru/html/form/accept-charset">htmlbook.ru</a>
     */
    @Attribute
    private String accept_charset;

    /**
     * @see <a href="http://htmlbook.ru/html/form/action">htmlbook.ru</a>
     */
    @Attribute
    private String action;

    /**
     * @see <a href="http://htmlbook.ru/html/form/autocomplete">htmlbook.ru</a>
     */
    @Attribute
    private String autocomplete;

    /**
     * @see <a href="http://htmlbook.ru/html/form/enctype">htmlbook.ru</a>
     */
    @Attribute
    private String enctype;

    /**
     * @see <a href="http://htmlbook.ru/html/form/method">htmlbook.ru</a>
     */
    @Attribute
    private String method;

    /**
     * @see <a href="http://htmlbook.ru/html/form/name">htmlbook.ru</a>
     */
    @Attribute
    private String name;

    /**
     * @see <a href="http://htmlbook.ru/html/form/novalidate">htmlbook.ru</a>
     */
    @Attribute(withoutValue = true)
    private Boolean novalidate;

    /**
     * @see <a href="http://htmlbook.ru/html/form/target">htmlbook.ru</a>
     */
    @Attribute
    private String target;

    public Form() {
        this("", "");
    }

    public Form(String method, String action){
        super("form");
        this.method = method;
        this.action = action;
    }

    public void setAutocomplete(boolean value){
        autocomplete = value ? "on" : "off";
    }

    public void addInput(String name, String type){
        addInput(name, type, "");
    }

    public void addInput(String name, String type, String value){
        Input input = new Input();
        input.setName(name);
        input.setType(type);
        if (value != null && !value.isEmpty()){
            input.setValue(value);
        }
        addInput(input);
    }

    public void addInput(Input input){
        addChild(input);
    }

    @Override
    public <T extends Tag> void addChild(T tag) throws IllegalArgumentException {
        super.addChild(tag);
    }
}
