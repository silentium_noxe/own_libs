package com.silentium_noxe.java.web.html.tags;

import com.silentium_noxe.java.web.html.Attribute;
import lombok.Getter;
import lombok.Setter;

/**
 * @see <a href="http://htmlbook.ru/html/style">htmlbook.ru</a>
 * @author silentium_noxe
 * TODO: implement read file
 */
@Getter
@Setter
public class Style extends Tag {

    /**
     * @see <a href="http://htmlbook.ru/html/style/media">htmlbook.ru</a>
     */
    @Attribute
    private String media;

    /**
     * @see <a href="http://htmlbook.ru/html/style/type">htmlbook.ru</a>
     */
    @Attribute
    private String type;

    private String content;

    public Style(){
        super("style");
    }
}
