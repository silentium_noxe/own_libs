package com.silentium_noxe.java.web.html.tags;

import com.silentium_noxe.java.web.html.Attribute;
import lombok.Getter;
import lombok.Setter;

/**
 * @see <a href="http://htmlbook.ru/html/p">htmlbook.ru</a>
 * @author silentium_noxe
 */
public class Paragraph extends Tag {
    @Getter
    private SimpleText text;

    /**
     * @see <a href="http://htmlbook.ru/html/p/align">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String align;

    public Paragraph() {
        this("");
    }

    public Paragraph(String value){
        this(new SimpleText(value));
    }

    public Paragraph(SimpleText value){
        super("p");
        this.text = value;
        addChild(this.text);
    }

    public void setText(String text){
        setText(new SimpleText(text));
    }

    public void setText(SimpleText value){
        removeChild(this.text);
        this.text = value;
        addChild(this.text);
    }
}
