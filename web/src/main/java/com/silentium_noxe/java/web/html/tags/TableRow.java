package com.silentium_noxe.java.web.html.tags;

import com.silentium_noxe.java.web.html.Attribute;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @see <a href="http://htmlbook.ru/html/tr">htmlbook.ru</a>
 * @author silentium_noxe
 */
public class TableRow extends Tag{

    /**
     * @see <a href="http://htmlbook.ru/html/tr/align">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String align;

    /**
     * @see <a href="http://htmlbook.ru/html/tr/bgcolor">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String bgcolor;

    /**
     * @see <a href="http://htmlbook.ru/html/tr/bordercolor">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String bordercolor;

    /**
     * @see <a href="http://htmlbook.ru/html/tr/char">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String attrChar;

    /**
     * @see <a href="http://htmlbook.ru/html/tr/charoff">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private Integer charoff;

    /**
     * @see <a href="http://htmlbook.ru/html/tr/valign">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String valign;

    private List<TableHead> heads;
    private List<TableData> dataList;

    public TableRow(){
        super("tr");
    }

    public TableRow add(TableHead head){
        if(heads == null){
            heads = new ArrayList<>();
        }
        addChild(head);
        heads.add(head);

        return this;
    }

    public TableRow add(TableData data){
        if(dataList == null){
            dataList = new ArrayList<>();
        }
        addChild(data);
        dataList.add(data);

        return this;
    }

    public void removeHead(int index){
        removeChild(heads.get(index));
        heads.remove(index);
    }

    public void removeData(int index){
        removeChild(dataList.get(index));
        dataList.remove(index);
    }
}
