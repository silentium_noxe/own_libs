package com.silentium_noxe.java.web.html.tags;

import com.silentium_noxe.java.web.html.Attribute;
import lombok.Getter;
import lombok.Setter;

/**
 * @see <a href="http://htmlbook.ru/html/link">htmlbook.ru</a>
 * @author silentium_noxe
 */
@Getter
@Setter
public class Link extends Tag {

    /**
     * @see <a href="http://htmlbook.ru/html/link/charset">htmlbook.ru</a>
     */
    @Attribute
    private String charset;

    /**
     * @see <a href="http://htmlbook.ru/html/link/href">htmlbook.ru</a>
     */
    @Attribute
    private String href;

    /**
     * @see <a href="http://htmlbook.ru/html/link/media">htmlbook.ru</a>
     */
    @Attribute
    private String media;

    /**
     * @see <a href="http://htmlbook.ru/html/link/rel">htmlbook.ru</a>
     */
    @Attribute
    private String rel;

    /**
     * @see <a href="http://htmlbook.ru/html/link/sizes">htmlbook.ru</a>
     */
    @Attribute
    private String sizes;

    /**
     * @see <a href="http://htmlbook.ru/html/link/type">htmlbook.ru</a>
     */
    @Attribute
    private String type;

    public Link(){
        this(null, null);
    }

    public Link(String rel, String href){
        super("link");
        this.rel = rel;
        this.href = href;
    }

    @Override
    public String toString() {
        return getOpeningTag();
    }
}
