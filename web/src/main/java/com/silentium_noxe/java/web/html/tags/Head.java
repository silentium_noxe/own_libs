package com.silentium_noxe.java.web.html.tags;

import com.silentium_noxe.java.web.html.Attribute;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Set;

/**
 * @see <a href="http://htmlbook.ru/html/head">htmlbook.ru</a>
 * @author silentium_noxe
 */
public class Head extends Tag {
    private Title title;

    //TODO: add tags Base, Basefont, Bgsound
    private List<Class<? extends Tag>> allowTags = List.of(
            Title.class,
            Meta.class,
            Link.class,
            Script.class,
            Style.class
    );

    /**
     * @see <a href="http://htmlbook.ru/html/head/profile">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String profile;

    public Head(){
        super("head");
    }

    /**
     * Can contain only:
     * {@link Link},
     * {@link Meta},
     * {@link Title},
     * {@link Script},
     * {@link Style}
     *
     * @throws IllegalArgumentException if body can't contain tag
     */
    @Override
    public <T extends Tag> void addChild(T tag) {
        if(!allowTags.contains(tag.getClass())){
            throw new IllegalArgumentException("Head can't contain "+tag.getClass().getSimpleName());
        }

        //Head can contain one tag title
        if(tag.getClass() == Title.class){
            title = (Title) tag;
            return;
        }

        super.addChild(tag);
    }

    @Override
    public void removeChild(Tag tag) {
        super.removeChild(tag);
    }

    @Override
    public String toString(){
        return  getOpeningTag()+
                (title == null ? "" : title)+
                getChildTagsAsString()+
                getClosingTag();
    }
}
