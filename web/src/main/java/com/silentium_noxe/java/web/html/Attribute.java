package com.silentium_noxe.java.web.html;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;

/**
 * @author silentium_noxe
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(value={FIELD})
public @interface Attribute {
    boolean withoutValue() default false;
}
