package com.silentium_noxe.java.web.html.tags;

import com.silentium_noxe.java.web.html.Attribute;
import lombok.Getter;
import lombok.Setter;

public class LineBreak extends Tag{

    @Getter
    @Setter
    @Attribute
    private String clear;

    public LineBreak(){
        super("br");
    }
}
