package com.silentium_noxe.java.web.html.tags;

import com.silentium_noxe.java.web.html.Attribute;
import lombok.Getter;
import lombok.Setter;

/**
 * @see <a href="http://htmlbook.ru/html/td">htmlbook.ru</a>
 * @author silentium_noxe
 */
@Getter
@Setter
public class TableData extends Tag{

    /**
     * @see <a href="http://htmlbook.ru/html/td/abbr">htmlbook.ru</a>
     */
    @Attribute
    private String abbr;

    /**
     * @see <a href="http://htmlbook.ru/html/td/align">htmlbook.ru</a>
     */
    @Attribute
    private String align;

    /**
     * @see <a href="http://htmlbook.ru/html/td/axis">htmlbook.ru</a>
     */
    @Attribute
    private String axis;

    /**
     * @see <a href="http://htmlbook.ru/html/td/background">htmlbook.ru</a>
     */
    @Attribute
    private String background;

    /**
     * @see <a href="http://htmlbook.ru/html/td/bgcolor">htmlbook.ru</a>
     */
    @Attribute
    private String bgcolor;

    /**
     * @see <a href="http://htmlbook.ru/html/td/bordercolor">htmlbook.ru</a>
     */
    @Attribute
    private String bordercolor;

    /**
     * @see <a href="http://htmlbook.ru/html/td/char">htmlbook.ru</a>
     */
    @Attribute
    private String attrChar;

    /**
     * @see <a href="http://htmlbook.ru/html/td/charoff">htmlbook.ru</a>
     */
    @Attribute
    private Integer charoff;

    /**
     * @see <a href="http://htmlbook.ru/html/td/colspan">htmlbook.ru</a>
     */
    @Attribute
    private Integer colspan;

    /**
     * @see <a href="http://htmlbook.ru/html/td/headers">htmlbook.ru</a>
     */
    @Attribute
    private String headers;

    /**
     * @see <a href="http://htmlbook.ru/html/td/height">htmlbook.ru</a>
     */
    @Attribute
    private Integer height;

    /**
     * @see <a href="http://htmlbook.ru/html/td/nowrap">htmlbook.ru</a>
     */
    @Attribute(withoutValue = true)
    private Boolean nowrap;

    /**
     * @see <a href="http://htmlbook.ru/html/td/rowspan">htmlbook.ru</a>
     */
    @Attribute
    private Integer rowspan;

    /**
     * @see <a href="http://htmlbook.ru/html/td/scope">htmlbook.ru</a>
     */
    @Attribute
    private String scope;

    /**
     * @see <a href="http://htmlbook.ru/html/td/valign">htmlbook.ru</a>
     */
    @Attribute
    private String valign;

    /**
     * @see <a href="http://htmlbook.ru/html/td/width">htmlbook.ru</a>
     */
    @Attribute
    private Integer width;

    private SimpleText text;

    public TableData(){
        this("");
    }

    public TableData(String text){
        this(new SimpleText(text));
    }

    public TableData(SimpleText text){
        super("td");
        this.text = text;
        addChild(text);
    }

    public void setText(String text){
        setText(new SimpleText(text));
    }

    public void setText(SimpleText value){
        removeChild(this.text);
        this.text = value;
        addChild(this.text);
    }
}
