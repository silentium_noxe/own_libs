package com.silentium_noxe.java.web.html.tags;

import com.silentium_noxe.java.web.html.Attribute;
import lombok.Getter;
import lombok.Setter;

/**
 * @see <a href="http://htmlbook.ru/html/button">htmlbook.ru</a>
 * @author silentium_noxe
 */
@Getter
@Setter
public class Button extends Tag {

    /**
     * @see <a href="http://htmlbook.ru/html/button/accesskey">htmlbook.ru</a>
     */
    @Attribute
    private String accesskey;

    /**
     * @see <a href="http://htmlbook.ru/html/button/autofocus">htmlbook.ru</a>
     */
    @Attribute(withoutValue = true)
    private Boolean autofocus;

    /**
     * @see <a href="http://htmlbook.ru/html/button/disabled">htmlbook.ru</a>
     */
    @Attribute(withoutValue = true)
    private Boolean disabled;

    /**
     * @see <a href="http://htmlbook.ru/html/button/form">htmlbook.ru</a>
     */
    @Attribute
    private String form;

    /**
     * @see <a href="http://htmlbook.ru/html/button/formaction">htmlbook.ru</a>
     */
    @Attribute
    private String formaction;

    /**
     * @see <a href="http://htmlbook.ru/html/button/formenctype">htmlbook.ru</a>
     */
    @Attribute
    private String formenctype;

    /**
     * @see <a href="http://htmlbook.ru/html/button/formmethod">htmlbook.ru</a>
     */
    @Attribute
    private String formmethod;

    /**
     * @see <a href="http://htmlbook.ru/html/button/formnovalidate">htmlbook.ru</a>
     */
    @Attribute
    private String formnovalidate;

    /**
     * @see <a href="http://htmlbook.ru/html/button/formtarget">htmlbook.ru</a>
     */
    @Attribute
    private String formtarget;

    /**
     * @see <a href="http://htmlbook.ru/html/button/name">htmlbook.ru</a>
     */
    @Attribute
    private String name;

    /**
     * @see <a href="http://htmlbook.ru/html/button/type">htmlbook.ru</a>
     */
    @Attribute
    private String type;

    /**
     * @see <a href="http://htmlbook.ru/html/button/value">htmlbook.ru</a>
     */
    @Attribute
    private String value;

    public Button() {
        super("button");
    }
}
