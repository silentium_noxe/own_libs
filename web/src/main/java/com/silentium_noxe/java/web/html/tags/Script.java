package com.silentium_noxe.java.web.html.tags;

import com.silentium_noxe.java.web.html.Attribute;
import lombok.Getter;
import lombok.Setter;

/**
 * @see <a href="http://htmlbook.ru/html/script">htmlbook.ru</a>
 * @author silentium_noxe
 * TODO: implement read file
 */
@Getter
@Setter
public class Script extends Tag {

    /**
     * @see <a href="http://htmlbook.ru/html/script/async">htmlbook.ru</a>
     */
    @Attribute(withoutValue = true)
    private Boolean async;

    /**
     * @see <a href="http://htmlbook.ru/html/script/defer">htmlbook.ru</a>
     */
    @Attribute(withoutValue = true)
    private Boolean defer;

    /**
     * @see <a href="http://htmlbook.ru/html/script/language">htmlbook.ru</a>
     */
    @Attribute
    private String language;

    /**
     * @see <a href="http://htmlbook.ru/html/script/src">htmlbook.ru</a>
     */
    @Attribute
    private String src;

    /**
     * @see <a href="http://htmlbook.ru/html/script/type">htmlbook.ru</a>
     */
    @Attribute
    private String type;

    private String content;

    public Script(){
        super("script");
    }
}
