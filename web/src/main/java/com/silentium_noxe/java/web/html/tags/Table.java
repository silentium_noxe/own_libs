package com.silentium_noxe.java.web.html.tags;

import com.silentium_noxe.java.web.html.Attribute;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @see <a href="http://htmlbook.ru/html/table">htmlbook.ru</a>
 * @author silentium_noxe
 */
public class Table extends Tag {

    /**
     * @see <a href="http://htmlbook.ru/html/table/align">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String align;

    /**
     * @see <a href="http://htmlbook.ru/html/table/background">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String background;

    /**
     * @see <a href="http://htmlbook.ru/html/table/bgcolor">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String bgcolor;

    /**
     * @see <a href="http://htmlbook.ru/html/table/border">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private Integer border;

    /**
     * @see <a href="http://htmlbook.ru/html/table/bordercolor">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String bordercolor;

    /**
     * @see <a href="http://htmlbook.ru/html/table/cellpadding">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private Integer cellpadding;

    /**
     * @see <a href="http://htmlbook.ru/html/table/cellspacing">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private Integer cellspacing;

    /**
     * @see <a href="http://htmlbook.ru/html/table/cols">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private Integer cols;

    /**
     * @see <a href="http://htmlbook.ru/html/table/frame">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String frame;

    /**
     * @see <a href="http://htmlbook.ru/html/table/height">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private Integer height;

    /**
     * @see <a href="http://htmlbook.ru/html/table/rules">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String rules;

    /**
     * @see <a href="http://htmlbook.ru/html/table/summary">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String summary;

    /**
     * @see <a href="http://htmlbook.ru/html/table/width">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private Integer width;

    private TableRow head;
    private List<TableRow> rows;

    public Table(){
        super("table");
        rows = new ArrayList<>();
    }

    public void setHead(String ... headers){
        head = new TableRow();
        for(String strHead : headers){
            head.add(new TableHead(new SimpleText(strHead)));
        }

        setHead(head);
    }

    public void setHead(TableRow head){
        if(head != null){
            removeChild(head);
        }

        addChild(head);
        this.head = head;
    }

    public void addRow(String ... data){
        TableRow row = new TableRow();

        for(String str : data){
            row.add(new TableData(new SimpleText(str)));
        }

        addRow(row);
    }

    public void addRow(TableRow row){
        addChild(row);
        this.rows.add(row);
    }

    public void addRows(List<TableRow> rows){
        for (TableRow row : rows){
            addRow(row);
        }
    }

    public void removeRow(int index){
        removeChild(rows.get(index));
        rows.remove(index);
    }
}
