package com.silentium_noxe.java.web.html.tags;


import com.silentium_noxe.java.web.html.Attribute;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

//TODO: implement event listeners
/**
 * @see <a href="http://htmlbook.ru/html/body">htmlbook.ru</a>
 * @author silentium_noxe
 */
public class Body extends Tag {

    /**
     * @see <a href="http://htmlbook.ru/html/body/alink">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String alink;

    /**
     * @see <a href="http://htmlbook.ru/html/body/background">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String background;

    /**
     * @see <a href="http://htmlbook.ru/html/body/bgcolor">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String bgcolor;

    /**
     * @see <a href="http://htmlbook.ru/html/body/bgproperties">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String bgproperties;

    /**
     * @see <a href="http://htmlbook.ru/html/body/bottommargin">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private Integer bottommargin;

    /**
     * @see <a href="http://htmlbook.ru/html/body/leftmargin">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private Integer leftmargin;

    /**
     * @see <a href="http://htmlbook.ru/html/body/link">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String link;

    /**
     * @see <a href="http://htmlbook.ru/html/body/rightmargin">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private Integer rightmargin;

    /**
     * @see <a href="http://htmlbook.ru/html/body/scroll">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private Boolean scroll;

    /**
     * @see <a href="http://htmlbook.ru/html/body/text">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String text;

    /**
     * @see <a href="http://htmlbook.ru/html/body/topmargin">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private Integer topmargin;

    /**
     * @see <a href="http://htmlbook.ru/html/body/vlink">htmlbook.ru</a>
     */
    @Getter
    @Setter
    @Attribute
    private String vlink;

    public Body(){
        super("body");

        banTags(Set.of(
                Body.class,
                Head.class,
                Html.class,
                Link.class,
                Meta.class,
                Title.class
        ));
    }

    /**
     * Can't contain tags:
     * {@link Body},
     * {@link Head},
     * {@link Html},
     * {@link Link},
     * {@link Meta},
     * {@link Title}
     *
     * @throws IllegalArgumentException if body can't contain tag
     */
    @Override
    public <T extends Tag> void addChild(T tag) {
        super.addChild(tag);
    }

    @Override
    public void removeChild(Tag tag){
        super.removeChild(tag);
    }
}
