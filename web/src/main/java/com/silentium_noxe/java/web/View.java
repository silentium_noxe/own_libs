package com.silentium_noxe.java.web;

import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * @author silentium_noxe
 */
public class View {

    private Map<String, String> elements;
    private ViewParams viewParams;

    public View() {
        viewParams = new ViewParams();
        elements = new HashMap<>();
    }

    /**
     * Default extension of file is HTML
     * @param nameTemplate name of file without extension
     * @throws IOException
     */
    public View addFromFile(String nameTemplate) throws IOException {
        return addFromFile(nameTemplate, viewParams.getDefaultFileExtension());
    }

    public View addFromFile(String nameTemplate, String fileExtension) throws IOException, IllegalArgumentException{
        if (fileExtensionIsWrong(fileExtension)){
            throw new IllegalArgumentException("Wrong file extension. Extension can't be null or empty");
        }

        if (!fileExtension.startsWith(".")){
            fileExtension = "."+fileExtension;
        }

        fileExtension = fileExtension.toLowerCase();

        File file = new File(viewParams.getPathResourcesDir() +nameTemplate+fileExtension);

        if (!file.exists()){
            throw new IOException("Template "+nameTemplate+" not found");
        }

        return add(nameTemplate, readFile(file));
    }

    public View add(String nameElement, String element) throws IllegalArgumentException{
        if (nameIsWrong(nameElement)){
            throw new IllegalArgumentException("Wrong element name. Name can't be null or empty");
        }

        if (elementIsWrong(element)){
            throw new IllegalArgumentException("Wrong element. Element can't be null or empty");
        }

        elements.put(nameElement, element);

        return this;
    }

    public void remove(String nameElement){
        elements.remove(nameElement);
    }

    public void update(String nameElement, String newElement){
        if (nameIsWrong(nameElement)){
            throw new IllegalArgumentException("Wrong element name. Name can't be null or empty");
        }

        if (elementIsWrong(newElement)){
            throw new IllegalArgumentException("Wrong element. Element can't be null or empty");
        }

        elements.replace(nameElement, newElement);
    }

    public String get(String nameElement){
        return elements.get(nameElement);
    }

    public String render(){
        StringBuilder result = new StringBuilder();

        for(String template : elements.keySet()){
            result.append(elements.get(template)).append("\n");
        }
        return result.toString();
    }

    private String readFile(File file) throws IOException{
        return new String(Files.readAllBytes(Paths.get(file.getPath())), StandardCharsets.UTF_8);
    }

    private boolean nameIsWrong(String string){
        return string == null || string.isEmpty();
    }

    private boolean elementIsWrong(String string){
        return string == null || string.isEmpty();
    }

    private boolean fileExtensionIsWrong(String string){
        return string == null || string.isEmpty();
    }

    public ViewParams getParams(){
        return viewParams;
    }

    public void setViewParams(ViewParams params){
        if (params == null){
            throw new IllegalArgumentException("View params null");
        }
        viewParams = params;
    }

    @Getter
    @Setter
    public class ViewParams {
        private String fileSeparator;
        private String pathResourcesDir;
        private String defaultFileExtension;

        public ViewParams() {
            fileSeparator = "/";
            pathResourcesDir =  "."+ fileSeparator +
                                "src"+ fileSeparator +
                                "main"+ fileSeparator +
                                "resources"+ fileSeparator;
            defaultFileExtension = "html";
        }
    }
}
