package com.silentium_noxe.java.web.html.tags;

import com.silentium_noxe.java.web.html.Attribute;
import lombok.Getter;
import lombok.Setter;

/**
 * @see <a href="http://htmlbook.ru/html/a">htmlbook.ru</a>
 */
@Getter
@Setter
public class HypertextLink extends Tag {

    /**
     * @see <a href="http://htmlbook.ru/html/a/accesskey">htmlbook.ru</a>
     */
    @Attribute
    private String accesskey;

    /**
     * @see <a href="http://htmlbook.ru/html/a/coords">htmlbook.ru</a>
     */
    @Attribute
    private String coords;

    /**
     * @see <a href="http://htmlbook.ru/html/a/download">htmlbook.ru</a>
     */
    @Attribute(withoutValue = true)
    private Boolean download;

    /**
     * @see <a href="http://htmlbook.ru/html/a/href">htmlbook.ru</a>
     */
    @Attribute
    private String href;

    /**
     * @see <a href="http://htmlbook.ru/html/a/hreflang">htmlbook.ru</a>
     */
    @Attribute
    private String hreflang;


    /**
     * @see <a href="http://htmlbook.ru/html/a/name">htmlbook.ru</a>
     */
    @Attribute
    private String name;

    /**
     * @see <a href="http://htmlbook.ru/html/a/rel">htmlbook.ru</a>
     */
    @Attribute
    private String rel;

    /**
     * @see <a href="http://htmlbook.ru/html/a/rev">htmlbook.ru</a>
     */
    @Attribute
    private String rev;

    /**
     * @see <a href="http://htmlbook.ru/html/a/shape">htmlbook.ru</a>
     */
    @Attribute
    private String shape;

    /**
     * @see <a href="http://htmlbook.ru/html/a/tabindex">htmlbook.ru</a>
     */
    @Attribute
    private Integer tabindex;

    /**
     * @see <a href="http://htmlbook.ru/html/a/target">htmlbook.ru</a>
     */
    @Attribute
    private String target;

    /**
     * @see <a href="http://htmlbook.ru/html/a/title">htmlbook.ru</a>
     */
    @Attribute
    private String title;

    /**
     * @see <a href="http://htmlbook.ru/html/a/type">htmlbook.ru</a>
     */
    @Attribute
    private String type;

    public HypertextLink() {
        this(null);
    }

    public HypertextLink(String href) {
        this(href, null);
    }

    public HypertextLink(String href, String rel) {
        this(href, rel, null);
    }

    public HypertextLink(String href, String rel, Boolean download) {
        super("a");
        this.download = download;
        this.href = href;
        this.rel = rel;
    }
}
