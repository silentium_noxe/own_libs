package com.silentium_noxe.java.web.html.tags;

import com.silentium_noxe.java.web.html.Attribute;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

/**
 * @see <a href="http://htmlbook.ru/html/div">htmlbook.ru</a>
 * @author silentium_noxe
 */
@Getter
@Setter
public class Div extends Tag {

    /**
     * @see <a href="http://htmlbook.ru/html/div/align">htmlbook.ru</a>
     */
    @Attribute
    private String align;

    /**
     * @see <a href="http://htmlbook.ru/html/div/title">htmlbook.ru</a>
     */
    @Attribute
    private String title;

    public Div() {
        this(null);
    }

    public Div(String id){
        super("div");
        setId(id);

        banTags(Set.of(
                Body.class,
                Head.class,
                Html.class,
                Link.class,
                Meta.class,
                Title.class
        ));
    }

    /**
     * Can't contain tags:
     * {@link Body},
     * {@link Head},
     * {@link Html},
     * {@link Link},
     * {@link Meta},
     * {@link Title}
     *
     * @throws IllegalArgumentException if body can't contain tag
     */
    @Override
    public <T extends Tag> void addChild(T tag){
        super.addChild(tag);
    }

    @Override
    public void removeChild(Tag tag){
        super.removeChild(tag);
    }
}
