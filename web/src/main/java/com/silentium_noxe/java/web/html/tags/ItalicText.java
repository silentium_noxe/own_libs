package com.silentium_noxe.java.web.html.tags;

import lombok.Getter;

/**
 * @see <a href="http://htmlbook.ru/html/i">htmlbook.ru</a>
 * @author silentium_noxe
 */
public class ItalicText extends Tag {
    @Getter
    private SimpleText text;

    public ItalicText() {
        this("");
    }

    public ItalicText(String value){
        this(new SimpleText(value));
    }

    public ItalicText(SimpleText value){
        super("i");
        this.text = value;
        addChild(this.text);
    }

    public void setText(String text){
        setText(new SimpleText(text));
    }

    public void setText(SimpleText value){
        removeChild(this.text);
        this.text = value;
        addChild(this.text);
    }
}
