import com.silentium_noxe.java.web.html.HtmlPageBuilder;
import com.silentium_noxe.java.web.html.tags.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TestHtmlPageBuilder {

    private static long startTime;
    private static long totalMemory;
    private static long resultMemory;

    @Before
    public void startTimer(){
        totalMemory = Runtime.getRuntime().totalMemory();
        startTime = System.currentTimeMillis();
    }

    @After
    public void endTimer(){
        long time = (System.currentTimeMillis() - startTime);
        System.out.println("( "+time+ " milliseconds or "+(time / 1000)+" seconds )");
        long memory = totalMemory - Runtime.getRuntime().freeMemory();
        System.out.println("( used memory (variant 1): "+memory+" bytes or "+(memory / 1024)+" Kbytes or "+(memory / 1024 / 1024)+" Mbytes )");
        System.out.println("( used memory (variant 2): "+resultMemory+" bytes or "+(resultMemory / 1024)+" Kbytes or "+(resultMemory / 1024 / 1024)+" Mbytes )");
        System.out.println("--------------------------");
    }

    @Test
    public void getHelloWorldPage(){
        System.out.println(getNameTest() + " -> ");

        String expected = "<!DOCTYPE html><html><head><title>Hello World!</title><meta charset='UTF-8'></head><body>Hello World!</body></html>";

        String strHelloWorld = "Hello World!";

        HtmlPageBuilder builder = new HtmlPageBuilder();
        builder.setTitle(strHelloWorld)
                .setCharset("UTF-8")
                .putIntoBody(new SimpleText(strHelloWorld));

        String result = builder.build();

        assertEquals("Wrong result", expected, result);

        snapshotMemory();
    }

    @Test
    public void testEasySearchingById(){
        System.out.println(getNameTest() + " -> ");

        HtmlPageBuilder builder = new HtmlPageBuilder();
        Div div = new Div();
        div.setId("div0");
        builder.putIntoBody(div);

        assertNotNull("Expected tag with id '"+div.getId()+"' not found", builder.getElementById(div.getId()));

        snapshotMemory();
    }

    @Test
    public void testMiddleSearchingById(){
        System.out.println(getNameTest() + " -> ");

        HtmlPageBuilder builder = new HtmlPageBuilder();
        Div div0 = new Div("div0");
        Div div1 = new Div("div1");
        Div div2 = new Div("div2");
        Div div3 = new Div("div3");
        Div div4 = new Div("div4");

        div0.addChild(div1);
        div1.addChild(div2);
        div2.addChild(div3);
        div3.addChild(div4);

        builder.putIntoBody(div0);

        assertNotNull("Expected tag with id '"+div4.getId()+"' not found", builder.getElementById(div4.getId()));

        snapshotMemory();
    }

    @Test
    public void testHardSearchingById(){
        System.out.println(getNameTest() + " -> ");

        HtmlPageBuilder builder = new HtmlPageBuilder();

        Div div1_1 = new Div(randId());
        Div div1_2 = new Div(randId());

        Div div2_1 = new Div(randId());
        div1_2.addChild(div2_1);

        div2_1.addChild(new Div(randId()));
        div2_1.addChild(new Div(randId()));
        Div target = new Div("here");
        Div div3_1 = new Div(randId());
        div3_1.addChild(target);
        div2_1.addChild(div3_1);
        div2_1.addChild(new Div(randId()));

        builder.putIntoBody(div1_1);
        builder.putIntoBody(div1_2);

        String id = fillDivAndGetIdLastDiv(div1_1, 2);
        Div div = (Div) builder.getElementById(id);
        fillDivAndGetIdLastDiv(div, 3);
        fillDivAndGetIdLastDiv(div, 2);


        assertNotNull("Expected tag with id '"+target.getId()+"' not found", builder.getElementById(target.getId()));

        snapshotMemory();
    }

    @Test
    public void buildTableByMatrix(){
        System.out.println(getNameTest() + " -> ");

        String expected = "<!DOCTYPE html><html><head></head><body><table><tr><th>id</th><th>key</th><th>value</th></tr><tr><td>0</td><td>hello</td><td>world</td></tr><tr><td>1</td><td>some</td><td>text</td></tr><tr><td>2</td><td>I</td><td>think</td></tr><tr><td>3</td><td>must</td><td>work</td></tr></table></body></html>";

        String[][] matrix = {
                {"id", "key", "value"},
                {"0", "hello", "world"},
                {"1", "some", "text"},
                {"2", "I", "think"},
                {"3", "must", "work"}
        };

        HtmlPageBuilder builder = new HtmlPageBuilder();
        builder.putIntoBody(builder.buildTable(matrix));

        assertEquals(expected, builder.build());

        snapshotMemory();
    }

    @Test
    public void buildTableArrayWave(){
        System.out.println(getNameTest() + " -> ");

        String expected = "<!DOCTYPE html><html><head></head><body><table><tr><th>id</th><th>key</th><th>value</th></tr><tr><td>0</td><td>hello</td><td>world</td></tr><tr><td>1</td><td>some</td><td>text</td></tr><tr><td>2</td><td>I</td><td>think</td></tr><tr><td>3</td><td>must</td><td>work</td></tr></table></body></html>";

        HtmlPageBuilder builder = new HtmlPageBuilder();

        String[] headers = {"id", "key", "value"};
        String[][] data = {
                {"0", "hello", "world"},
                {"1", "some", "text"},
                {"2", "I", "think"},
                {"3", "must", "work"}
        };

        builder.putIntoBody(builder.buildTable(headers, data));

        assertEquals(expected, builder.build());

        snapshotMemory();
    }

    @Test
    public void buildTableLongWave(){
        System.out.println(getNameTest() + " -> ");

        String expected = "<!DOCTYPE html><html><head></head><body><table><tr><th>id</th><th>key</th><th>value</th></tr><tr><td>0</td><td>hello</td><td>world</td></tr><tr><td>1</td><td>some</td><td>text</td></tr><tr><td>2</td><td>I</td><td>think</td></tr><tr><td>3</td><td>must</td><td>work</td></tr></table></body></html>";

        HtmlPageBuilder builder = new HtmlPageBuilder();

        //Set header
        TableRow headRow = new TableRow()
                .add(new TableHead("id"))
                .add(new TableHead("key"))
                .add(new TableHead("value"));

        //Put data
        List<TableRow> dataList = new ArrayList<>();
        TableRow row = new TableRow()
                .add(new TableData("0"))
                .add(new TableData("hello"))
                .add(new TableData("world"));

        dataList.add(row);

        row = new TableRow()
                .add(new TableData("1"))
                .add(new TableData("some"))
                .add(new TableData("text"));

        dataList.add(row);

        row = new TableRow()
                .add(new TableData("2"))
                .add(new TableData("I"))
                .add(new TableData("think"));

        dataList.add(row);

        row = new TableRow()
                .add(new TableData("3"))
                .add(new TableData("must"))
                .add(new TableData("work"));

        dataList.add(row);

        builder.putIntoBody(builder.buildTable(headRow, dataList));

        assertEquals(expected, builder.build());

        snapshotMemory();
    }

    @Test
    public void buildForm(){
        System.out.println(getNameTest() + " -> ");

        String expected = "<form action='some_url' method='GET'><input name='test-1' type='text'><input type='submit'></form>";

        HtmlPageBuilder builder = new HtmlPageBuilder();
        Form form = builder.buildForm("GET", "some_url", new ArrayList<Input>(){{
            add(new Input("test-1", "text"));
            add(new Input("", "submit"));
        }});

        snapshotMemory();

        assertEquals(expected, form.toString());
    }

    @Test
    public void buildOrderedList(){
        System.out.println(getNameTest() + " -> ");

        String expected = "<ol><li>hello</li><li>world</li></ol>";

        HtmlPageBuilder builder = new HtmlPageBuilder();
        OrderedList list = builder.buildOrderedList("hello", "world");

        snapshotMemory();

        assertEquals(expected, list.toString());
    }

    private String fillDivAndGetIdLastDiv(Div div, int deep){
        Div container = new Div(randId());
        div.addChild(container);

        for(int i = 1; i <= deep; i++){
            Div nextDiv = new Div(randId());
            container.addChild(nextDiv);
            container = nextDiv;
        }
        return container.getId();
    }

    private String randId(){
        return UUID.randomUUID().toString();
    }

    private static void snapshotMemory(){
        resultMemory = totalMemory - Runtime.getRuntime().freeMemory();
    }

    private static String getNameTest(){
        return Thread.currentThread().getStackTrace()[2].getMethodName();
    }
}
