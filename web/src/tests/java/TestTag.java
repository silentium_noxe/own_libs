import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestTag {

    @Test(expected = IllegalArgumentException.class)
    public void nullTagName(){
        OpenTagForTest tag = new OpenTagForTest(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void emptyTagName(){
        OpenTagForTest tag = new OpenTagForTest("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void putNullChild(){
        OpenTagForTest tag = new OpenTagForTest("test");
        tag.addChild(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void putExistId(){
        OpenTagForTest tag = new OpenTagForTest("test");
        OpenTagForTest tag1 = new OpenTagForTest("test1");
        OpenTagForTest tag2 = new OpenTagForTest("test2");

        tag1.setId("testID");
        tag2.setId("testID");

        tag.addChild(tag1);
        tag.addChild(tag2);
    }

    @Test
    public void getOpeningTagWithoutAttributes(){
        String expected = "<test>";
        OpenTagForTest tag = new OpenTagForTest("test");
        String result = tag.getOpeningTag();
        assertEquals("Got "+result+" (Expected: "+expected+")", expected, result);
    }

    @Test
    public void getOpeningTagWithAttributeId(){
        String expected = "<test id='id1'>";
        OpenTagForTest tag = new OpenTagForTest("test");
        tag.setId("id1");
        String result = tag.getOpeningTag();
        assertEquals("Got "+result+" (Expected: "+expected+")", expected, result);
    }

    @Test
    public void getClosingTag(){
        String expected = "</test>";
        OpenTagForTest tag = new OpenTagForTest("test");
        String result = tag.getClosingTag();
        assertEquals("Got "+result+" (Expected: "+expected+")", expected, result);
    }

    @Test
    public void getSimpleBlock(){
        String expected = "<test><test1></test1><test2><test3></test3></test2></test>";
        OpenTagForTest tag = new OpenTagForTest("test");
        tag.addChild(new OpenTagForTest("test1"));
        OpenTagForTest tag2 = new OpenTagForTest("test2");
        tag2.addChild(new OpenTagForTest("test3"));
        tag.addChild(tag2);
        String result = tag.toString();
        assertEquals("Got "+result+"\n(Expected: "+expected+")", expected, result);
    }
}
