import com.silentium_noxe.java.web.html.HtmlPageBuilder;
import com.silentium_noxe.java.web.html.tags.Table;

public class TestMain {
    public static void main(String[] args) {
        HtmlPageBuilder htmlPageBuilder = new HtmlPageBuilder();
        Table table = new Table();

        table.setHead("test1", "test2", "test3");
        table.addRow("hello", "world", "!");
        System.out.println(table);
    }
}
