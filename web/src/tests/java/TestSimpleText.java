import com.silentium_noxe.java.web.html.tags.Div;
import com.silentium_noxe.java.web.html.tags.SimpleText;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestSimpleText {

    @Test
    public void returnText(){
        String expected = "hello";

        assertEquals(expected, new SimpleText(expected).toString());
    }

    @Test
    public void testToString(){
        String expected = "Hello <div id='test'></div> World!";

        SimpleText simpleText = new SimpleText("Hello [test] World!");
        simpleText.addVariable("test", new Div("test"));

        assertEquals(expected, simpleText.toString());
    }
}
