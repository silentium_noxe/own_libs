import com.silentium_noxe.java.web.html.Attribute;
import com.silentium_noxe.java.web.html.tags.Tag;
import lombok.Getter;
import lombok.Setter;

public class OpenTagForTest extends Tag {

    public OpenTagForTest(String name) {
        super(name);
    }

    @Override
    public <T extends Tag> void addChild(T tag) {
        super.addChild(tag);
    }

    @Override
    public void removeChild(Tag tag) {
        super.removeChild(tag);
    }

    @Override
    public void saveId(String id) {
        super.saveId(id);
    }

    @Override
    public String getOpeningTag(){
        return super.getOpeningTag();
    }

    @Override
    public String getClosingTag() {
        return super.getClosingTag();
    }

    @Override
    public String getChildTagsAsString() {
        return super.getChildTagsAsString();
    }

    @Override
    public String getAttributesAsString(){
        return super.getAttributesAsString();
    }

    @Override
    public boolean isWrong(Object object) {
        return super.isWrong(object);
    }

    @Override
    public String attributeToString(String atrName, String value) {
        return super.attributeToString(atrName, value);
    }

    
}
